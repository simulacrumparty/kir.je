const getRandomInt = (max) => Math.floor(Math.random() * max);

const generateSpecks = (number, container, style) => {
  if (!container) return;

  container.style.position = 'relative';

  const containerWidth = container.getBoundingClientRect().width;
  const containerHeight = container.getBoundingClientRect().height;

  for (let i = 0; i < number; i++) {
    const span = document.createElement('span');
    span.classList.add('speck', `speck--${style}`);

    span.style.left = getRandomInt(containerWidth);
    span.style.top = getRandomInt(containerHeight);

    container.appendChild(span);
  }
};

generateSpecks(2000, document.body, 'dark');

const logo = document.querySelector('.header__logo');
generateSpecks(500, logo, 'light');

const title = document.querySelector('.post-header');
generateSpecks(100, title, 'light');
